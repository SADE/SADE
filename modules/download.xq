xquery version "3.1";
(:~
 : This module handles the download of the current SADE application on publish.html.
 : The current (maybe forked) SADE app is provided as an installable XAR package.
 :
 : @author unknown
 : @author Michelle Weidling
 : @version 1.0
 :)

declare namespace pkg="http://expath.org/ns/pkg";

import module namespace dbutil="http://exist-db.org/xquery/dbutil";
import module namespace config="https://sade.textgrid.de/ns/config" at "config.xqm";

(:~
 : The main download function. Pack the SADE app denoted in $app-collection
 : into a XAR file and then offer it as a download on publish.html.
 :
 : @param $app-collection The path to the application that is to be downloaded
 : @param $expathConf The root element of the application's expath-pkg.xml
 : @param $expand-xincludes true() if XIncludes should be expanded automatically
 :   
 :)
declare function local:download($app-collection as xs:string, $expathConf as element(pkg:package), $expand-xincludes as xs:boolean) as empty-sequence() {
    let $name := concat($expathConf/@abbrev, "-", $expathConf/@version, ".xar")
    let $entries :=
        dbutil:scan(xs:anyURI($app-collection), function($collection as xs:anyURI?, $resource as xs:anyURI?) {
            let $resource-relative-path := substring-after($resource, $app-collection || "/")
            let $collection-relative-path := substring-after($collection, $app-collection || "/")
            return
                if (empty($resource)) then
                    (: no need to create a collection entry for the app's root directory :)
                    if ($collection-relative-path eq "") then
                        ()
                    else
                        <entry type="collection" name="{$collection-relative-path}"/>
                else if (util:binary-doc-available($resource)) then
                    <entry type="uri" name="{ $resource-relative-path }">{ $resource }</entry>
                else
                    <entry type="xml" name="{ $resource-relative-path }">{
                        util:declare-option("exist:serialize", "expand-xincludes=" || (if ($expand-xincludes) then "yes" else "no")),
                        doc($resource)
                    }</entry>
        })
    let $xar := compression:zip($entries, true())
    return (
        response:set-header("Content-Disposition", concat("attachment; filename=", $name)),
        response:stream-binary($xar, "application/zip", $name)
    )
};

let $collection := $config:app-root
let $expathConf := xmldb:xcollection($collection)/*:package
let $expand-xincludes := request:get-parameter("expand-xincludes", "false") cast as xs:boolean

return
local:download($collection, $expathConf, $expand-xincludes)
