xquery version "3.1";

(: This library module contains XQSuite tests for the callgraph module
 : stored in callgraph.xqm :)

module namespace callgraph-test = "https://sade.textgrid.de/ns/callgraph-test";

import module namespace callgraph="https://sade.textgrid.de/ns/callgraph" at "../callgraph.xqm";

declare namespace graphml="http://graphml.graphdrawing.org/xmlns";
declare namespace test="http://exist-db.org/xquery/xqsuite";

declare
    %test:name("Testing filename error message by error code")
    %test:args("invalid-filename.svg")
    %test:assertError("CALLGRAPH01")
    function callgraph-test:aa-test-error($option as xs:string) {
        callgraph:main($option, "test-2")
};

declare
    %test:name("Testing option error message by error code")
    %test:args("an invalid option")
    %test:assertError("CALLGRAPH02")
    function callgraph-test:aa-test-error($option as xs:string) {
        callgraph:main($option, "test-2")
};

declare
    %test:name("Testing error message by error description")
    %test:args("an invalid option")
    %test:assertError("Invalid option 'an invalid option'. Please enter a base URI, a collection path or 'full' as argument.")
    function callgraph-test:ab-test-error($option as xs:string) {
        callgraph:main($option, "test-2")
};


declare
    %test:name("Test DOT output from graphml:graphml")
    %test:args("<graphml xmlns=""http://graphml.graphdrawing.org/xmlns""/>")
    %test:assertEquals("digraph g {", "}")
    function callgraph-test:transform2dot-001($element as element()) {
        callgraph:transform2dot($element)
};


declare
    %test:name("Test DOT output from graphml:graph")
    %test:args("<graph xmlns=""http://graphml.graphdrawing.org/xmlns"" id=""base-graph""><node id=""some-function"" /></graph>")
    %test:assertEmpty    
    
    %test:args("<graph xmlns=""http://graphml.graphdrawing.org/xmlns""/>")
    %test:assertEquals("node [shape=box, style=filled, color=""", "#90b0d4", """];")
    function callgraph-test:transform2dot-002($element as element()) {
        callgraph:transform2dot($element)
};


declare
    %test:name("Test DOT output from graphml:node")
    %test:args("<graph xmlns=""http://graphml.graphdrawing.org/xmlns""><node id=""some-function""/></graph>")
    %test:assertEquals("node [shape=box, style=filled, color=""", "#90b0d4", """];", """", "some-function", """ [shape=box];")    
    
    %test:args("<graph xmlns=""http://graphml.graphdrawing.org/xmlns"" id=""base-graph""><node id=""some-function""/></graph>")
    %test:assertEmpty
    function callgraph-test:transform2dot-003($element as element()) {
        callgraph:transform2dot($element)
};


declare
    %test:name("Test DOT output from graphml:edge")
    %test:args("<edge xmlns=""http://graphml.graphdrawing.org/xmlns"" source=""some-function"" target=""another-function""/>")
    %test:assertEquals("""", "some-function", """", " -&gt; ", """", "another-function", """;")
    function callgraph-test:transform2dot-004($element as element()) {
        callgraph:transform2dot($element)
};


declare
    %test:name("Testing DOT ouput for a single module")
    %test:args("/db/apps/sade/modules/callgraph/tests/xqm/inspect2graphml-testmodule.xqm")
    %test:assertEquals("digraph g {node [shape=box, style=filled, color=""#90b0d4""];""inspect2graphml-testmodule:get-value"" [shape=box];""inspect2graphml-testmodule:main"" [shape=box];""inspect2graphml-testmodule:set-value"" [shape=box];""inspect2graphml-testmodule:main"" -> ""inspect2graphml-testmodule:get-value"";""inspect2graphml-testmodule:main"" -> ""inspect2graphml-testmodule:set-value"";""inspect2graphml-testmodule:set-value"" -> ""inspect2graphml-testmodule:get-value"";}")
    function callgraph-test:ba-dot($option as xs:string) {
        callgraph:get-dot($option, "inspect2graphml-testmodule-xqm")
};


declare
    %test:name("Testing DOT ouput for two modules")
    %test:args("/db/apps/sade/modules/callgraph/tests/xqm/")
    %test:assertEquals("digraph g {node [shape=box, style=filled, color=""#90b0d4""];""inspect2graphml-testmodule2:get-value"" [shape=box];""inspect2graphml-testmodule2:main"" [shape=box];""inspect2graphml-testmodule2:set-value"" [shape=box];""inspect2graphml-testmodule2:main"" -&gt; ""inspect2graphml-testmodule2:get-value"";""inspect2graphml-testmodule2:main"" -&gt; ""inspect2graphml-testmodule2:set-value"";""inspect2graphml-testmodule2:main"" -&gt; ""inspect2graphml-testmodule:set-value"";""inspect2graphml-testmodule2:set-value"" -&gt; ""inspect2graphml-testmodule:get-value"";node [shape=box, style=filled, color=""#e6f2ff""];""inspect2graphml-testmodule:get-value"" [shape=box];""inspect2graphml-testmodule:main"" [shape=box];""inspect2graphml-testmodule:set-value"" [shape=box];""inspect2graphml-testmodule:main"" -&gt; ""inspect2graphml-testmodule:get-value"";""inspect2graphml-testmodule:main"" -&gt; ""inspect2graphml-testmodule:set-value"";""inspect2graphml-testmodule:set-value"" -&gt; ""inspect2graphml-testmodule:get-value"";}")
    function callgraph-test:bb-dot($option as xs:string) {
        callgraph:get-dot($option, "tests-collection")
};

declare
    %test:name("Testing SVG error behavior")
    %test:args("")
    %test:assertError("CALLGRAPH06")
    function callgraph-test:c-svg($dot as xs:string) {
        callgraph:get-svg($dot)
};


declare
    %test:name("Testing SVG clearance for a simple module")
    %test:args("/db/apps/sade/modules/callgraph/tests/xqm/inspect2graphml-testmodule.xqm")
    %test:assertEquals("Call graph creation successfull.")
    function callgraph-test:d-svg($option as xs:string) {
        callgraph:main($option, "inspect2graphml-testmodule-xqm2")
};
