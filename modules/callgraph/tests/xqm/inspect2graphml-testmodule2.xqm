xquery version "3.1";

module namespace inspect2graphml-testmodule2="https://sade.textgrid.de/ns/inspect2graphml-test-module2";

import module namespace inspect2graphml-testmodule="https://sade.textgrid.de/ns/inspect2graphml-test-module" at "inspect2graphml-testmodule.xqm";


declare function inspect2graphml-testmodule2:main() as xs:boolean {
    let $url := "some-url"
    let $url-return := inspect2graphml-testmodule2:get-value($url)
    let $set-value1 := inspect2graphml-testmodule2:set-value($url)
    let $set-value2 := inspect2graphml-testmodule:set-value($url)
    return
        true()
};

declare function inspect2graphml-testmodule2:get-value($url as xs:string) as xs:string {
    "return value"
};

declare function inspect2graphml-testmodule2:set-value($url as xs:string) as empty-sequence() {
    let $value := inspect2graphml-testmodule:get-value($url)
    return
        ()
};