xquery version "3.1";

module namespace inspect2graphml-testmodule="https://sade.textgrid.de/ns/inspect2graphml-test-module";

declare function inspect2graphml-testmodule:main() as xs:boolean {
    let $url := "some-url"
    let $url-return := inspect2graphml-testmodule:get-value($url)
    let $set-value := inspect2graphml-testmodule:set-value($url)
    return
        true()
};

declare function inspect2graphml-testmodule:get-value($url as xs:string) as xs:string {
    "return value"
};

declare function inspect2graphml-testmodule:set-value($url as xs:string) as empty-sequence() {
    let $value := inspect2graphml-testmodule:get-value($url)
    return
        ()
};