xquery version "3.1";

(: This library module contains XQSuite tests for the inspect2graphml module
 : stored in inspect2graphml.xqm :)

module namespace inspect2graphml-test = "https://sade.textgrid.de/ns/inspect2graphml-test";

import module namespace inspect2graphml="https://sade.textgrid.de/ns/inspect2graphml" at "../inspect2graphml.xqm";

declare namespace graphml="http://graphml.graphdrawing.org/xmlns";
declare namespace test="http://exist-db.org/xquery/xqsuite";


declare
    %test:name("Testing the test")
    %test:args("some input")
    %test:assertEmpty
    function inspect2graphml-test:a-test-the-test($node as text()) {
        ()
};


declare
    %test:name("Testing a simple module with module node as input")
    %test:args("<module uri=""http://test.de/portal/test"" prefix=""test"" location=""/db/apps/sade/modules/test.xqm""><description>Some Documentation</description><author> Michelle Weidling </author><version> 1.0 </version><function name=""test:determine-dir"" module=""http://test.de/portal/test""><argument type=""xs:string"" cardinality=""exactly one"" var=""filename"">the name of the current file</argument><returns type=""xs:string"" cardinality=""exactly one"">xs:string The path where the HTML is stored</returns><description>Determines the directory where an HTML file should be saved.</description><version> 1.0 </version><author> Michelle Weidling </author><calls><function name=""another-test:assure-dir-available"" module=""http://test.de/ns/another-test"" arity=""1""/></calls></function></module>")
    %test:assertEquals("<graphml:graph xmlns:graphml=""http://graphml.graphdrawing.org/xmlns"" id=""test"" edgedefault=""directed""><graphml:node id=""test:determine-dir""/><graphml:edge  source=""test:determine-dir"" target=""another-test:assure-dir-available""/><graphml:node id=""another-test:assure-dir-available""/></graphml:graph>")
    function inspect2graphml-test:ba-simple-graph($node as element(*)) {
        inspect2graphml:transform($node, true())
};

declare
    %test:name("Testing a simple module with URI as input")
    %test:args("/db/apps/sade/modules/callgraph/tests/xqm/inspect2graphml-testmodule.xqm")
    %test:assertEquals("<graphml:graphml xmlns:graphml=""http://graphml.graphdrawing.org/xmlns"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd""><graphml:graph id=""inspect2graphml-testmodule"" edgedefault=""directed""><graphml:node id=""inspect2graphml-testmodule:get-value""/><graphml:node id=""inspect2graphml-testmodule:main""/><graphml:node id=""inspect2graphml-testmodule:set-value""/><graphml:edge source=""inspect2graphml-testmodule:main"" target=""inspect2graphml-testmodule:get-value""/><graphml:edge source=""inspect2graphml-testmodule:main"" target=""inspect2graphml-testmodule:set-value""/><graphml:edge source=""inspect2graphml-testmodule:set-value"" target=""inspect2graphml-testmodule:get-value""/></graphml:graph></graphml:graphml>", "/db/apps/sade/modules/callgraph/graphml/inspect2graphml-test-graphml.xml")
    function inspect2graphml-test:bb-simple-graph($option as xs:string) {
        inspect2graphml:main($option, "inspect2graphml-test")
};


declare
    %test:name("Testing two simple modules with collection path as input")
    %test:args("/db/apps/sade/modules/callgraph/tests/xqm/")
    %test:assertEquals("<graphml:graphml xmlns:graphml=""http://graphml.graphdrawing.org/xmlns"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd""><graphml:graph id=""base-graph"" edgedefault=""directed""><graphml:node id=""3b57287f3dcdc4f5308965b41b581acf""><graphml:graph id=""inspect2graphml-testmodule2"" edgedefault=""directed""><graphml:node id=""inspect2graphml-testmodule2:get-value""/><graphml:node id=""inspect2graphml-testmodule2:main""/><graphml:node id=""inspect2graphml-testmodule2:set-value""/><graphml:edge source=""inspect2graphml-testmodule2:main"" target=""inspect2graphml-testmodule2:get-value""/><graphml:edge source=""inspect2graphml-testmodule2:main"" target=""inspect2graphml-testmodule2:set-value""/><graphml:edge source=""inspect2graphml-testmodule2:main"" target=""inspect2graphml-testmodule:set-value""/><graphml:edge source=""inspect2graphml-testmodule2:set-value"" target=""inspect2graphml-testmodule:get-value""/></graphml:graph></graphml:node><graphml:node id=""bc0c0e51d61b52ff839322816df6050e""><graphml:graph id=""inspect2graphml-testmodule"" edgedefault=""directed""><graphml:node id=""inspect2graphml-testmodule:get-value""/><graphml:node id=""inspect2graphml-testmodule:main""/><graphml:node id=""inspect2graphml-testmodule:set-value""/><graphml:edge source=""inspect2graphml-testmodule:main"" target=""inspect2graphml-testmodule:get-value""/><graphml:edge source=""inspect2graphml-testmodule:main"" target=""inspect2graphml-testmodule:set-value""/><graphml:edge source=""inspect2graphml-testmodule:set-value"" target=""inspect2graphml-testmodule:get-value""/></graphml:graph></graphml:node></graphml:graph></graphml:graphml>", "/db/apps/sade/modules/callgraph/graphml/inspect2graphml-test2-graphml.xml")
    function inspect2graphml-test:bc-simple-graph($option as xs:string) {
        inspect2graphml:main($option, "inspect2graphml-test2")
};

declare
    %test:name("Testing transformation from GraphML to DOT with one module")
    %test:args("/db/apps/sade/modules/callgraph/tests/xqm/inspect2graphml-testmodule.xqm")
    %test:assertEquals("<graphml:graphml xmlns:graphml=""http://graphml.graphdrawing.org/xmlns"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd""><graphml:graph id=""inspect2graphml-testmodule"" edgedefault=""directed""><graphml:node id=""inspect2graphml-testmodule:get-value""/><graphml:node id=""inspect2graphml-testmodule:main""/><graphml:node id=""inspect2graphml-testmodule:set-value""/><graphml:edge source=""inspect2graphml-testmodule:main"" target=""inspect2graphml-testmodule:get-value""/><graphml:edge source=""inspect2graphml-testmodule:main"" target=""inspect2graphml-testmodule:set-value""/><graphml:edge source=""inspect2graphml-testmodule:set-value"" target=""inspect2graphml-testmodule:get-value""/></graphml:graph></graphml:graphml>", "/db/apps/sade/modules/callgraph/graphml/inspect2graphml-test3-graphml.xml")
    function inspect2graphml-test:ca-simple-graphml-to-dot($xqm as xs:string) {
        inspect2graphml:main($xqm, "inspect2graphml-test3")
};
