xquery version "3.1";
(:~
 : This script generates a copy of the current application, renames it and
 : deploys the package into the same database.
 : Afterwards the client is redirected to the same page the request started
 : but in the new application.
 :
 : @author Mathias Göbel
 : @author Stefan Hynek
 : @author Michelle Weidling
 : @version 0.3
 : :)

import module namespace dbutil="http://exist-db.org/xquery/dbutil";
import module namespace config="https://sade.textgrid.de/ns/config" at "/db/apps/sade/modules/config.xqm";

declare namespace cf="https://sade.textgrid.de/ns/configfile";
declare namespace expath="http://expath.org/ns/pkg";
declare namespace repo="http://exist-db.org/xquery/repo";

(:~
 : Generates a random password in the given length. For compatibility reasons
 : only ASCII characters are used. A password MUST have at least 10 characters.
 : It is strongly recommended to change the password to a personalized one!
 :
 : @param $length the length of the resulting password
 : @author Mathias Göbel
 : @author Michelle Weidling
 : @return A password of the requested length composed of ASCII characters
 : @since 0.2
 :  :)
declare function local:passwordGenerator($length as xs:integer) as xs:string {
  if($length lt 10) then
      error( QName("https://sade.textgrid.de/ns/error", "FORK04"), "Short password. Use at least 10 characters.")
  else
    (: charmap must not contain characters that do not evaluate against xs:anyURI
      or a regular expression REPLACEMENT. :)
    let $charset := string-to-codepoints('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz') => codepoints-to-string()
    let $charsetLength := string-length($charset)
    let $password :=
      for $i in 1 to $length
      let $num := util:random($charsetLength)
      return
        substring($charset, $num, 1)
    return
      string-join($password)
};

declare variable $target := request:get-parameter("target", "koma");
declare variable $password := local:passwordGenerator(15);
declare variable $referer := request:get-header("Referer");
declare variable $dry-run := request:get-parameter("dry", "false");

declare variable $expathConf :=
    <package xmlns="http://expath.org/ns/pkg" name="https://sade.textgrid.de/ns/SADE/{ $target }" abbrev="SADE-{ $target }" version="0.1" spec="1.0">
        <title>{ $target }</title>
        <dependency package="http://exist-db.org/apps/shared"/>
        <dependency package="https://sade.textgrid.de/ns/assets"/>
    </package>;

declare variable $repoConf :=
    <meta xmlns="http://exist-db.org/xquery/repo">
        <description>{ $target }. A Fork of TextGrid: SADE.</description>
        <author>Johannes Biermann</author>
        <author>Mathias Göbel</author>
        <author>Stefan Hynek</author>
        <author>Markus Matoni</author>
        <author>Ubbo Veentjer</author>
        <author>Michelle Weidling</author>
        <website>https://sade.textgrid.de</website>
        <status>alpha</status>
        <license>GNU-LGPL</license>
        <copyright>true</copyright>
        <type>application</type>
        <target>sade-{ $target }</target>
        <permissions password="{ $password }" user="{ $target }" group="sade{ $target }" mode="rw-r--r--"/>
        <prepare>pre-install.xq</prepare>
        <finish>post-install.xq</finish>
    <deployed>{ current-dateTime() }</deployed>
    </meta>;


(:~
 : Creates a XAR package of the newly forked SADE application.
 :
 : @param $app-collection The path to the newly created collection, e.g. ""/db/tmp/SADE-test"
 : @param $expand-xincludes true() if eXist's XInclude expanding mechanism should be switched on
 : @return A XAR package containing the newly forked SADE application
 : @author Mathias Göbel
 : @author Michelle Weidling
 :)
declare function local:xar($app-collection as xs:string, $expand-xincludes as xs:boolean)
as xs:base64Binary* {
    let $entries :=
        dbutil:scan(xs:anyURI($app-collection), function($collection as xs:anyURI?, $resource as xs:anyURI?) {
            let $resource-relative-path := substring-after($resource, $app-collection || "/")
            let $collection-relative-path := substring-after($collection, $app-collection || "/")
            return
                if (empty($resource)) then
                    (: no need to create a collection entry for the app's root directory :)
                    if ($collection-relative-path eq "") then
                        ()
                    else
                        <entry type="collection" name="{$collection-relative-path}"/>
                else if (util:binary-doc-available($resource)) then
                    <entry type="uri" name="{$resource-relative-path}">{ $resource }</entry>
                else
                    <entry type="xml" name="{$resource-relative-path}">{
                        util:declare-option("exist:serialize", "expand-xincludes=" || (if ($expand-xincludes) then "yes" else "no")),
                        doc($resource)
                    }</entry>
        })
    let $xar := compression:zip($entries, true())
    return
        $xar
};

let $listTargetsNotAllowed := ("develop", xmldb:get-child-collections('/db/apps')[starts-with(., "sade-")] ! substring-after(., "sade-"))

return
    if( $target = "" ) then
        error( QName("https://sade.textgrid.de/ns/error", "FORK01"), "No target provided.")
    else if ( $target = $listTargetsNotAllowed ) then
        error( QName("https://sade.textgrid.de/ns/error", "FORK05"), "The fork must not be namend " || string-join($listTargetsNotAllowed, "; ") || ".")
    else if ( $expathConf/string(@name) = (//expath:package/string(@name)) ) then
        error( QName("https://sade.textgrid.de/ns/error", "FORK06"), "Target collection is available, but target name is not.")
    else
        let $expand-xincludes := request:get-parameter("expand-xincludes", "false") cast as xs:boolean
        let $name := string($expathConf/@abbrev)
        let $xar-name := $name || "-" || string($expathConf/@version) || ".xar"
        let $source-collection := if(starts-with($config:app-root, "null")) then substring-after($config:app-root, "null") else $config:app-root
        let $source-name := tokenize($source-collection, "/")[last()]
        let $login := xmldb:login(
                "/db",
                request:get-parameter("forkUser", ""),
                request:get-parameter("forkPassword", "")
            )
        return
            if(not($login)) then
                error( QName("https://sade.textgrid.de/ns/error", "FORK03"), "Authentication not successful. :-(")
            else
                let $prepare as xs:string :=
                    (
                        xmldb:create-collection(xmldb:create-collection("/db", "tmp"), $name),
                        xmldb:copy-collection($source-collection, "/db/tmp/" || $name)
                    )
                let $replace-the-configs :=
                (
                    xmldb:store("/db/tmp/" || $name || "/" || $source-name, "repo.xml", $repoConf),
                    xmldb:store("/db/tmp/" || $name || "/" || $source-name, "expath-pkg.xml", $expathConf)
                )
                let $edit-config.xml :=
                    update replace doc( "/db/tmp/" || $name || "/" || $source-name || "/config.xml" )//cf:param[@key = "project-title"]/text() with text {replace($target, "\-", " ")}
                let $path-to-xar as xs:string :=
                    xmldb:store-as-binary(
                        "/db/tmp/"||$name,
                        $xar-name,
                        (: $prepare results in two xs:string (which are identical.
                        thus we have to select one of them. :)
                        xs:base64Binary(local:xar( $prepare[1] || "/" || $source-name, $expand-xincludes))
                    )
                return
                    if($dry-run = "true")
                    then
                        (: this error is not an error, but in case of a dry run, we want to get
                        a status information, that tells us that it was a dry run. :)
                        error( QName("https://sade.textgrid.de/ns/error", "FORK02"), "Dry run completed.")
                    else
                        (: we are using the repo addon because it handles all permissions
                           correct. :)
                        let $complete := repo:install-and-deploy-from-db($path-to-xar)

                        return
                            if( $complete/@result = "ok" ) then
                              let $replacement := "apps/" || replace($name, "SADE", "sade") || "/publish.html?newpw=" || $password
                              let $uri := replace(tokenize($referer, "\?")[1], "apps/.+/publish\.html", $replacement)
                              return
                                response:redirect-to(xs:anyURI($uri))
                            else
                                $complete
