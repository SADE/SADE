xquery version "3.1";

(:~
 : This module handles the main level navigation of SADE. The entries can be
 : changed at navigation.xml. While each item is rendered as a clickable button,
 : submenus are displayed as labels for dropdown menus.
 :
 : This module is called by templates/nav_include.html.
 :
 : Please note that submenus within submenus aren't allowed since they aren't
 : regarded as good practice anymore.
 :
 : @author Mathias Göbel
 : @author Stefan Hynek
 : @author Ubbo Veentjer
 : @author Michelle Weidling
 : @version 1.1
:)

module namespace nav="https://sade.textgrid.de/ns/navigation";

import module namespace app="https://sade.textgrid.de/ns/app" at "app.xqm";
import module namespace config="https://sade.textgrid.de/ns/config" at "config.xqm";
import module namespace templates="http://exist-db.org/xquery/templates";

declare variable $nav:module-key := "navigation";
declare variable $nav:lang := app:getLanguage();
declare variable $nav:langDefault := (config:get("lang.default", "multilanguage"));
declare variable $nav:langTest :=   if($nav:langDefault = "")
                                    then true()
                                    else ($nav:lang = $nav:langDefault);

declare
    %templates:wrap
function nav:navitems($node as node(), $model as map(*)) as map(*)
{
    let $confLocation := config:get("location", $nav:module-key)
    let $navitems := doc( $config:app-root || "/" || $confLocation)//navigation/*
    return
        map { "navitems" : $navitems }
};


(:~
 : Handle the main buttons in the navigation, e.g. "About SADE". For each child
 : of navigation in navigation.xqm, <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-template="nav:head"> is evoked and processed.
 :
 : The whole process considers the currently selected language for the label creation.
 :
 : @author Mathias Göbel
 : @author Stefan Hynek
 : @author Ubbo Veentjer
 : @author Michelle Weidling
 :
 : @param $node The current xhtml:a (which is then further processed)
 : @param $model A map containing arbitrary data – used to pass information between template calls
 : @return element() with the label text in the right language
 :)
declare function nav:head($node as node(), $model as map(*)) as element()
{
    (: $model("item") is the current child of the navigation entity in navigation.xml :)
    switch(local-name($model("item")))
        case "submenu" return
            element { node-name($node) } {
                    $node/@*,
                    if ($nav:langTest) then
                        string($model("item")/@label)
                    else
                        string($model("item")/@*[local-name( . ) = "label-" || $nav:lang]),
                    $node/node()
            }
        case "item" return
            element { node-name($node) } {
                    attribute href {$model("item")/@link},
                    if(contains(string($model("item")/@link), ":")
                    or string($model("item")/@link) = "info.html") then
                        ()
                    else
                        attribute target {"_blank"},
(:                    if ($nav:langTest or not($model("subitem")/@*[local-name() ="label-" || $nav:lang])):)
                    if ($nav:langTest) then
                        string($model("item")/@label)
                    else
                        string($model("item")/@*[local-name( . ) = "label-" || $nav:lang])
            }
        default return
            <b>not defined: { node-name($model("item")) }</b>
};


(:~
 : Create a map of all elements that are displayed as part of a dropdown.
 : These are encoded in navigation.xml as nodes with a depth level of 2, i.e.
 : children of the root node's children.
 :
 : @author Mathias Göbel
 : @author Stefan Hynek
 : @author Ubbo Veentjer
 : @author Michelle Weidling
 :
 : @param $node The current <ul class="dropdown-menu" data-template="nav:subitems">...</ul> which is evoked once for each child of the element 'navigation'
 : @param $model A map containing arbitrary data - used to pass information between template calls
 : @return A map containing all nodes with a depth level of 2
 :)
declare
    %templates:wrap
function nav:subitems($node as node(), $model as map(*)) as map(*)
{
    map{ "subitems" : $model("item")/*}
};


(:~
 : Process and serialize the nodes that are part of a dropdown.
 :
 : @author Mathias Göbel
 : @author Stefan Hynek
 : @author Ubbo Veentjer
 : @author Michelle Weidling
 :
 : @param $node The current <a href="#" data-template="nav:subitem" /> that is evoked once for each node with a depth level of 2
 : @param $model A map containing arbitrary data - used to pass information between template calls
 : @return An XHTML element with either a link with a label in the current language or an empty span
 :)
declare function nav:subitem($node as node(), $model as map(*)) as element(*)
{
    if($model("subitem")/@class) then
        <span class="{$model("subitem")/@class}">&#160;</span>
    else if ($model("subitem")/name() != 'divider') then
        element a {
            if(string($model("subitem")/@link)) then attribute href { string($model("subitem")/@link)} else (),
            if(contains(string($model("subitem")/@link), ":")) then
                ()
            else
                attribute target {"_blank"},
            if ($nav:langTest
            or not($model("subitem")/@*[local-name() ="label-" || $nav:lang])) then
                string($model("subitem")/@label)
            else
                string($model("subitem")/@*[local-name( . ) = "label-" || $nav:lang])
        }
    else <span>&#160;</span>
};
