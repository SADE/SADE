xquery version "3.1";
(:~
 : This module takes care of all user interface related matters of searching:
 :
 :   * search
 :   * display results
 :   * find and show facets
 :   * apply filters (from facets)
 :   * apply sort order
 :   * pagination
 :
 : This module needs a working lucene index and is configured in main config.xml.
 : It makes use of the template engine and has related template files.
 :
 : @author Ubbo Veentjer
 : @author Mathias Göbel
 : @version 2.0
 : :)
module namespace fsearch="https://sade.textgrid.de/ns/faceted-search";

import module namespace config="https://sade.textgrid.de/ns/config" at "config.xqm";
import module namespace kwic="http://exist-db.org/xquery/kwic";

declare namespace bol="http://blumenbach-online.de/blumenbachiana";
declare namespace cf="https://sade.textgrid.de/ns/configfile";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace templates="http://exist-db.org/xquery/templates";

(:~ Helper for templating, returns the actual search term.
 : This method replaces its html node (no %templates-wrap)
 :
 : Usage: <div>Sie haben nach <strong>
 :          <span id="prevQuery">
 :            <span data-template="fsearch:return-query"/>
 :          </span>
 :        </strong> gesucht.</div>
 :
 : Output:
 :        <div>Sie haben nach <strong>
 :          <span id="prevQuery">könig</span>
 :        </strong> gesucht.</div>
 :
 : @param $node
 : @param $model
 :)
declare function fsearch:return-query($node as node(), $model as map(*), $q as xs:string?)
as xs:string? {
    $q
};

declare function fsearch:results($node as node(), $model as map(*), $page as xs:string) as map()* {
    let $page := (if($page) then $page else 1) cast as xs:integer
    let $target := $config:app-root || "/" || config:get("project-id") || "/data"
    let $hits := local:get-hits($model, $target)

    let $obreq := request:get-parameter("order-by", "relevance")
(:    let $order-by := string-join($config:configDoc//module[@key="faceted-search"]//order[@key = $obreq]//xpath , ","):)
    let $hitsordered :=
        if(request:get-parameter("order", "descending") = "descending") then
                for $hit in $hits
(:                order by util:eval($order-by) descending:)
                return $hit
            else
                for $hit in $hits
(:                order by util:eval($order-by) ascending:)
                return $hit

    let $num :=  try{ xs:integer(config:get("hits-per-page", "faceted-search")) } catch * {0}
    let $pages := try { ceiling(count($hits) div $num) } catch * { 0 }
    let $start := $page * $num - $num + 1

    return
        map {
            "facets" : fsearch:facets($model, $hits),
            "hits" : subsequence($hitsordered,$start,$num),
            "totalhits" : count($hits),
            "start" : $start,
            "pagination" : local:pagination(xs:integer($page), xs:integer($pages))
        }
};

declare
function fsearch:searchquery($node as node(), $model as map(*)) {
    let $query := request:get-parameter("q", ())
    return <input type="text" value="{$query}" name="q" class="form-control" />
};

declare
    %templates:wrap
function fsearch:hitcount($node as node(), $model as map(*)) {
    $model("totalhits")
};

declare
    %templates:wrap
function fsearch:hitstart($node as node(), $model as map(*)) {
    $model("start")
};

declare
    %templates:wrap
function fsearch:hitend($node as node(), $model as map(*)) {
    let $num := try { xs:integer(config:get("hits-per-page", "faceted-search")) } catch * {0}
    let $res := $model("start") + $num - 1
    return if($res > $model("totalhits"))
        then $model("totalhits")
        else $res
};

declare function fsearch:result-title($node as node(), $model as map(*)) {
    let $viewdoc := config:get('viewer-html', 'faceted-search')

    return
        <a href="{$viewdoc}{util:document-name($model("hit"))}">
            {
                for $titleQuery in config:get("result-title", "faceted-search")//cf:xpath
                return util:eval("($model('hit')" || $titleQuery || ")[1]")
            }
        </a>
};

declare function fsearch:result-xslt($node as node(), $model as map(*)) {

    let $docname := util:document-name($model("hit"))
    let $id := substring-before($docname, ".")
    let $link := config:get('viewer-html','faceted-search') || $docname
    let $xslt := $config:app-root || "/" || config:get('result-xslt','faceted-search')

    let $docpath := config:get("data-dir") || "/xml/data/" || $docname
    let $title := doc($docpath)//tei:fileDesc/tei:titleStmt/tei:title

    return
        transform:transform($model("hit"), doc($xslt),
            <parameters>
                <param name="id" value="{$id}"/>
                <param name="link" value="{$link}"/>
                <param name="title" value="{$title}"/>
            </parameters>
        )

};

declare function fsearch:result-score($node as node(), $model as map(*)) {
    ft:score($model('hit'))
};

declare function fsearch:result-img($node as node(), $model as map(*)) {

    ()

(:    (:  TODO: could better be done in template, than by param :):)
(:    if(xs:boolean(config:get( "thumbnail","faceted-search"))) then:)
(:        (: TODO image-xpath should be configured by conf.xml:):)
(:        let $src := $model("hit")//bol:kerndaten/bol:mediafiles//bol:mediafile[1]/@file_uri || ($model("hit")//tei:pb)[1]/@facs:)
(:        return:)
(:            if(string-length($src) > 0) then:)
(:                <img src="{config:get("digilib.url")}{$src}{config:get("digilib.thumbnailparam")}"/>:)
(:            else:)
(:                ():)
(:        else:)
(:            ():)
};

declare function fsearch:result-kwic($node as node(), $model as map(*)) {

    let $hit := $model("hit")
    let $expanded := kwic:expand($hit)
    let $kwic-width := config:get("kwic-width", "faceted-search")
    order by ft:score($hit) descending
    return
        for $i in 1 to xs:integer(config:get("kwic-hits", "faceted-search"))
        return
            if(($expanded//exist:match)[$i]) then
                let $summary := kwic:get-summary($expanded, ($expanded//exist:match)[$i], <config width="{$kwic-width}"/>)
                return
                    <p class="kwic"><span class="kwic-prev">{$summary/span[@class="previous"]/node()}</span> <span class="kwic-hit">{$summary/span[@class="hi"]/node()}</span> <span>{$summary/span[@class="following"]/node()}</span></p>
            else ()
};

declare function fsearch:result-source($node as node(), $model as map(*)) {

    let $docloc := config:get("data-dir") || "/xml/data/" || util:document-name($model("hit"))
    return
        <a href="/exist/rest{$docloc}">{ $node/@class, $node/* }</a>
};

declare
(:    %templates:wrap:)
function fsearch:result-id($node as node(), $model as map(*)) {
    element { node-name($node) } { attribute name { util:document-name($model("hit")) }, $node/@*, $node/* }
};

declare function fsearch:facets($model as map(*), $hits) as map() {
    map:merge(
        for $facet in config:get("facets", "faceted-search")
        return
            map:entry($facet/string(@key), local:facet($model, $hits, $facet/string(@key), string($facet/cf:xpath/text())))
    )
};

declare function fsearch:list-facets($node as node(), $model as map(*)) as map(*){
    map { "facetgroups" : $model("facet") }
};

declare
function fsearch:facet-title($node as node(), $model as map(*)) {
    <li>{ map:keys($model("facet")) }</li>
};

declare function fsearch:facet($node as node(), $model as map(*)) as item()* {
    let $facets := config:get("facets", "faceted-search")
    for $facet in $facets
      (: hide facet-categories with less than one entry :)
      where (count($model("facets")($facet/string(@key))) > 0)
      return
          <li><strong>{ xs:string($facet/@title) }</strong>
              <ul class="hideMore">{ local:deselected-for-key($model, xs:string($facet/@key)) }{ $model("facets")(xs:string($facet/@key)) }</ul>
          </li>
};

declare function local:facet($model as map(*), $hits as node()*, $key as xs:string, $types as xs:string*) as node()* {

    let $query := request:get-parameter("q", ())
    let $order := request:get-parameter("order", ())
    let $order-by := request:get-parameter("order-by", ())
    let $facetReq := local:facet-query-from-request()

    (: the link to the html page displaying this module :)
    let $search-html := tokenize(request:get-url(), "/")[last()]

    (:construct xpath, e.g. $hits//tei:persName | $hits//bol:a0 :)
    let $fqueries := for $type in $types
        return concat("$hits", "//", $type)

    let $fquery := string-join($fqueries, " |")

    (: normalize strings :)
    let $unnormal := for $x in util:eval($fquery)
        return
            (:let $norms := normalize-space($x):)
            let $norms := xs:string($x)
            return if (string-length($norms) >= 1) then
                <un>{ $norms }</un>
            else
                ()

    let $deselected := local:deselected-for-key($model, $key)

    for $facet in distinct-values($unnormal)
        let $freq :=  count($unnormal[. eq $facet])
        order by $freq descending
        return
            if(local:facetSelected($key, $facet))
               then
                   let $facetRemoveQuery := replace($facetReq, $key || ":" || xmldb:encode($facet) || "," , "")
                   return
                       <li class="facetSelected">
                            <a class="facet-minus" href="{ $search-html }?q={ $query }&amp;facet={ $facetRemoveQuery }&amp;order={ $order }&amp;order-by={ $order-by }&amp;page=1"><i class="fas fa-minus"></i> </a>
                            { $facet } ({ $freq })
                        </li>
               else
                   <li>
                       <a class="facet-minus" href="{ $search-html }?q={ $query }&amp;facet={ $key }:!{ xmldb:encode($facet) },{ $facetReq }&amp;order={ $order }&amp;order-by={ $order-by }&amp;page=1"><i class="fas fa-minus" title="exclude"></i></a>
                       <a title="exclude all others" href="{ $search-html }?q={ $query }&amp;facet={ $key }:{ xmldb:encode($facet) },{ $facetReq }&amp;order={ $order }&amp;order-by={ $order-by }&amp;page=1">{ $facet } </a> ({ $freq })
                   </li>

};

declare function local:deselected-for-key($model, $key as xs:string) {

    let $query := request:get-parameter("q", ())
    let $order := request:get-parameter("order", ())
    let $order-by := request:get-parameter("order-by", ())

    (: the link to the html page displaying this module :)
    let $search-html := tokenize(request:get-url(), "/")[last()]

    let $facetReq := local:facet-query-from-request()

    for $fparts in tokenize($facetReq, ",")
        let $parts := tokenize($fparts, ":")
        return if($parts[1] eq $key) then
            if(starts-with($parts[2], "!"))
                    then
                        let $facetRemoveQuery := replace($facetReq, $key || ":" || $parts[2] || "," , "")
                        return
                            <li class="facet-deselected">
                                <a class="facet-plus" href="{$search-html}?q={$query}&amp;facet={$facetRemoveQuery}&amp;order={$order}&amp;order-by={$order-by}&amp;page=1">
                                  <i class="fa fa-plus"></i>
                                </a>
                                {" " || xmldb:decode(substring-after($parts[2], "!"))}
                            </li>
                    else
                        ()
            else ()
};

declare function local:get-hits($model as map(*), $target as xs:string) as node()*{

    let $query := request:get-parameter("q", ())
    let $fxquery := local:construct-facet-query($model)

    let $options :=
        <options>
            <leading-wildcard>yes</leading-wildcard>
        </options>
    let $xqueries :=
            for $query-root in config:get("query-root", "faceted-search")
            return
                if($query) then
                    "collection($target)" || $query-root || $fxquery || "[ft:query(., $query, $options)]"
                else
                    "collection($target)" || $query-root || $fxquery

    let $xquery := string-join($xqueries, " | ")

    return util:eval($xquery)
};

declare function local:construct-facet-query($model as map(*)) as xs:string {

    let $facet := local:facet-query-from-request()
    let $fxquery :=
        if ($facet) then
            for $fquery in tokenize($facet, ",")
                let $parts := tokenize($fquery, ":")
                let $select :=
                    for $xpath in config:get("facets", "faceted-search")[@key = $parts[1]]//cf:xpath
                        let $val := xmldb:decode($parts[2])
                        let $op := if(starts-with($val, "!"))
                                  then " ne "
                                  else " eq "
                        let $val := replace($val, "!", "")
                        return if(starts-with($xpath, ".") or starts-with($xpath, "/"))
                            (: to escpae quotes, you have to double them :)
                            then $xpath || $op || """" || $val || """"
                            else ".//" || $xpath || $op || """" || $val || """"

                return
                    if(not(empty($select))) then "[" || string-join($select, " or ") || "]"
                    else ()
        else
            ()

    return string-join($fxquery, "")
};

(: we need the facet uri encoded, so request-get parameter does not work :)
declare function local:facet-query-from-request() {
    (: remove trailing questionary mark :)
    let $querystring := substring(request:get-query-string(),1)
    (: tokenize at ampersand :)
    return for $part in tokenize($querystring, codepoints-to-string(38))
        let $query := tokenize($part, "=")
            return if ($query[1] eq "facet") then
                $query[2]
            else
                ()

};

declare function local:facetSelected($key as xs:string, $value as xs:string)
as xs:boolean {

    let $r := for $token in tokenize(local:facet-query-from-request(), ",")
        let $parts := tokenize($token, ":")
                return if($parts[1]=$key and xmldb:decode($parts[2])=$value)
                    then true()
                else ()

    return boolean($r)
};

(:~
 : Create the pagination list in xhtml, made for bootstrap 3. As local function
 : to be added to the $model, so can be added to the page several times.
 : @since 2.0
 : @author Mathias Göbel
 :)
declare function local:pagination($page as xs:integer, $pages as xs:integer)
as element(ul) {
  let $disableFirst := if($page = 1)
                   then attribute class {"disabled"}
                   else ()
  let $disableLast := if($page = $pages)
                    then attribute class {"disabled"}
                    else ()
  let $pagesListItems :=
    for $i in 1 to $pages
    let $title :=
      if($page = $i)
      then "Current page is " || $i
      else "Go to page " || $i
    let $disableCurrent :=
        if($page = $i)
        then attribute class {"disabled"}
        else ()
    let $queryString := request:get-query-string() => replace("page=\d+", "page=" || $i)
    let $prefixEllipsis := if(($page - $i eq  2) and $pages ne $i and $i ne 1) then "…" else ()
    let $suffixEllipsis := if(($page - $i eq -2) and $pages ne $i and $i ne 1) then "…" else ()
    where $page - $i lt 3
    where $page - $i gt -3
    return
      <li>
        {$disableCurrent}
        <a href="?{ $queryString }" title="{$title}">
          { $prefixEllipsis }{ $i }{ $suffixEllipsis }
        </a>
      </li>

  return
  <ul class="pagination">
      <li>{$disableFirst}<a href="?{ request:get-query-string() => replace("page=\d+", "page=1") }" title="Go to first page">&lt;&lt;</a></li>
      <li>{$disableFirst}<a href="?{ request:get-query-string() => replace("page=\d+", "page=" || ($page - 1)) }" title="Go to previous page">&lt;</a></li>
      { $pagesListItems }
      <li>{$disableLast}<a href="?{ request:get-query-string() => replace("page=\d+", "page=" || ($page + 1)) }" title="Go to next page">&gt;</a></li>
      <li>{$disableLast}<a href="?{ request:get-query-string() => replace("page=\d+", "page=" || $pages) }" title="Go to last page">&gt;&gt;</a></li>
  </ul>
};

(:~
 : Templating function for pagination.
 : @see local:pagination
 : @since 2.0
 : @author Mathias Göbel
 :)
declare function fsearch:pagination($node as node(), $model as map(*)) {
  $model("pagination")
};
