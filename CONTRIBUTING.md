# How to contribute

We appreciate any contribution from a third-party. We know that it is hard to
get into a long-grown workflow, but we want to keep it as simple as possible.
By following this guide, you will increase the possibility to get your commits
in the source code immediately.

Please **do not** use any feature of our source code hosting facility for personal
support requests. Send these requests either to our
[public mailing list](https://listserv.gwdg.de/mailman/listinfo/sade) or – if you
want to keep the communication in private – send a mail to [the owner of the
mailing list](mailto:sade-owner@sub.uni-goettingen.de).

## Issue tracker
It begins with a ticket on the [issue
tracker](https://gitlab.gwdg.de/SADE/SADE/issues). This is a good place to start
a discussion about your request and we can establish a basic collaboration. If
you do not have an account there, do not hesitate to
[register](https://gwdg.de/registration). Because this is a rather generic account,
we need a short information when you received your user name to add your account
to GitLab. Please send it to [sade-owner@sub.uni-goettingen.de](mailto:sade-owner@sub.uni-goettingen.de).

Currently you have two issue templates at your disposal: [Bug](https://gitlab.gwdg.de/SADE/SADE/blob/master/.gitlab/issue_templates/Bug.md) and [Feature]((https://gitlab.gwdg.de/SADE/SADE/blob/master/.gitlab/issue_templates/Feature-request.md). Please
use one of these to create your issue to provide all information we need to swiftly
getting things done.

A ticket is not necessary for [trivial
changes](https://puppet.com/community/trivial-patch-exemption-policy#general-guidance-for-using-this-policy).

The discussion at the issue tracker might lead to the decision that your
contribution should be placed in an external library or application. Do not
worry about that. We will do our best to integrate the library as a dependency. In
some cases it is more important to keep the core slim.

Always follow the discussion there and take part as much as you can. Questions
that are left unanswered for more than two weeks will result in a rejection of
the issue.

## Fork
Fork the repository. Start your development on top of the develop branch.

## Apply changes
Following the recommendation to use the [RDD
styleguides](https://github.com/subugoe/rdd-technical-reference/blob/master/rdd-technical-reference.md)
will improve the acceptance of the resulting pull request.

Make commits of logical and atomic units. Squash multiple trivial commits into a
single commit. Check for unnecessary whitespace with `git diff --check` before
committing. Avoid multi-line commit messages and keep the message clear, simple
and precise. When you see yourself writing “minor changes” or any even shorter
form, as stated above you have to squash multiple trivial commits into a single
commit.

Make sure you have added the necessary tests for your changes.

Make sure you have added an appropriate documentation for your changes. This
includes necessary updates of present chapters.

If your proposed changes include a new feature, do not forget to extend the
[feature list](docs/features.md).

## Pull requests (PR)
Push all your changes to the fork you have created.

Create a MR via the web-interface. Double-triple check that the target branch is
ORIGIN/develop.

Within the title of the PR name the ticket by using the `#` symbol and the
ticket number. When the changes will solve an issue completely, please write
`closes` of `fixes` before the ticket reference.

Merge requests resulting in test pipeline failures will not be accepted – but
you can always ask for assistance.

After feedback has been given we expect responses within two weeks. After two
weeks we may close the PR if it isn't showing any activity.

## Internal Workflow
This part is dedicated to the internal workflow and reflects what is considered
to be best practice at Research and Development at SUB Göttingen. For an
extensive description please consult the [RDD Technical Reference](https://github.com/subugoe/rdd-technical-reference/).

### git flow
We are using [`git flow`](https://danielkummer.github.io/git-flow-cheatsheet/).
This means that all developments will be reviewed before they will be merged to
the `develop` branch. The `develop` branch is the default one.

When a branch is dedicated to a ticket, the branch name should start with the
number of the ticket and match a pattern like `(bugfix|hotfix|feature)/#\d+-[a-z\-]`.

All issues will be arranged in [milestones](https://gitlab.gwdg.de/groups/SADE/-/milestones). Milestones are
always group-wide, so we combine tickets from all projects at the [SADE group](https://gitlab.gwdg.de/groups/SADE)
to a single milestone.

All projects in the SADE group are using [branch protection](https://docs.gitlab.com/ee/user/project/protected_branches.html) ([local documentation](https://gitlab.gwdg.de/help/user/project/protected_branches))
for `master` and `develop` branches. No one is allowed to push
directly to these branches except for the release workflow. To be able to
merge develop to master locally and to use the git flow command line tools, both
branches will be set to **unprotected** (allow to push for maintainers) for this
single action.

### Merge requests (MR)

Merge requests are peer reviewed before merging them into `develop`.
Please choose a person you see fit as assignee.
Always squash commit your MR and make sure the source branch (hotfix/bugfix/feature) is deleted after the MR has been accepted.

In case an assignee wants something to be changed in the MR, the MR is reassigned to the original reviser of the issue.
After implementing (or declining) the desired suggestions, the MR is reassigned to the original assignee.

If a merge conflict occurs the person who has proposed the MR is responsible for solving all conflicts.

### Release
New features and bugfixes will be added to new releases. [Here](https://gitlab.gwdg.de/SADE/SADE/-/releases)
you can find an overview of all releases.

#### How to set up a new release
Following the *git flow* new releases will be prepared with the CLI tool [gitflow-avh](https://github.com/petervanderdoes/gitflow-avh)).
+ the currently active [milestone](https://gitlab.gwdg.de/groups/SADE/-/milestones) names the version number of SADE/SADE only! For SADE/assets, SADE/build and all others please look at the version with `grep --max-count=1 project.version < build.properties`
  * is there a sufficient number of issues closed?
  * move open issues to next milestone
+ remove the [branch protection](https://gitlab.gwdg.de/SADE/SADE/-/settings/repository#js-protected-branches-settings) to enable push requests to develop and master (otherwise all merges have to be done at GitLab and not locally)
+ `git flow release start '[VERSION]'` to create the release branch.
+ set the version number according to the milestone
+ `git flow release finish '[VERSION]'` to merge the release branch into master and develop.
+ add Release Notes: The first commit message (`.git/MERGE_MSG`) the command above will ask for will become the release description.
  * write in markdown and escape `#` with `\#`
  * insert a headline and write a short paragraph about this release
  * insert sections for features and bugfixes and list all of them using the issue reference `#[ISSUENUMBER]`
+ it is possible to use the same text for the following tag and commit message
+ after successfully finishing the release process, the version for develop should be increased by a PATCH ([SemVer](https://semver.org/#backusnaur-form-grammar-for-valid-semver-versions)). please commit this change.
+ send all of this to the remote `git push --all && git push --tags`
+ reset the [branch protection](https://gitlab.gwdg.de/SADE/SADE/-/settings/repository#js-protected-branches-settings)

#### GitLab Releases
GitLab can maintain releases via API only. Therefore a CI job is used parsing the
merge message, preserving the build artifact and preparing the release.

### Meetings
Once in a month all team members will meet at an informal (and usually internal)
meeting to discuss issues and proceedings. If external participants are interested
in, they can contact us via [this e-mail address](mailto:sade-owner@sub.uni-goettingen.de).

## Further readings
This guide is inspired by [Puppetlabs/puppet/CONTRIBUTING.md](https://github.com/puppetlabs/puppet/blob/74c759f43f99789455ca52a26eeee20a7f6e4782/CONTRIBUTING.md) and [thoughtbot/factory_bot_rails/CONTRIBUTING.md](https://github.com/thoughtbot/factory_bot_rails/blob/c5d11518d7168ac28c80cee3d55e0d7e251dc49b/CONTRIBUTING.md). Both are mentioned in a [blog article at GitHub](https://blog.github.com/2012-09-17-contributing-guidelines/).
