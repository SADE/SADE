# Images

SADE offers two ways for dealing with images:

1. using TextGrid's image Server (recommended)
2. storing images as binary data directly in the database

## TextGrid Image Server
SADE utilizes eXist-db to store and process XML data; but apart from the
capability of storing any binary data in the database, processing and caching of
image data should be done outside of the database. Fortunately TextGrid offers
an instance of [DigiLib to process image data from the TextGrid Repository](https://dev.textgridlab.org/doc/services/submodules/tg-iiif-metadata/docs_tgrep/index.html).

### Publish image data
For SADE this means that no image data has to be transferred to the database.
**Publishing image data is disabled by default**, but a list of all images
(URI, title and format) offered to the database is stored in a document.
This list of URIs is used by the IIIF-Proxy tool. The publisher's output in this
case is `Image: some metadata stored.`. Developers may edit the function
`local:image-store($meta)` at `connect.xqm` and its corresponding call to store
binary data.

### IIIF-Proxy
The `iiif-proxy.xq` is a module for displaying image data from the TextGrid
Repository via a local URL. It receives the URI as a GET-parameter. On a local
instance the URL [http://localhost:8080/exist/apps/sade/modules/textgrid/iiif-proxy.xq?id=textgrid:18wz6](http://localhost:8080/exist/apps/sade/modules/textgrid/iiif-proxy.xq?id=textgrid:18wz6)
will return a image.

Images in the list described above are also available via the short URL:
[http://localhost:8080/exist/apps/sade/textgrid:18wz6](http://localhost:8080/exist/apps/sade/textgrid%3A18wz6).
This is handled by the `controller.xql`.
