# TextGrid Clients

There are XQuery functions that are able to communicate with the REST and SOAP interfaces of TextGrid
available in `/modules/textgrid/`. They are mainly used to READ data from any
(public|private) part of the repository – see [publish.md](publish.md) –,
but can be used to do any other operation, e.g. manipulating data, creating data
and more. A possible use case is the
logging of information in a file available in TextGrid -- so it will be
independent from your installation and will survive an `apt purge` on the server.
You can use SADE to cleanup data, re-write files, put some information about
validation to the project you and your team is working on, etc.

For description of the available functions, see the XQdocs annotations or have
a look at the visualization on any running server at [`fundocs.html`](http://localhost:8080/exist/apps/sade/fundocs.html).
