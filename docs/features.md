# Features

## General
* professional template
* easy customizing
* [multi-language support](multilingual.md)
* [wiki parser](wikiParser.md)
  * synchronize selected wiki pages
  * for [Confluence](confluenceParser.md)
  * for [DokuWiki](dokuwikiParser.md)
* all features from [exist-db](https://exist-db.org), e.g. REST interface
* a [highlight.js](https://highlightjs.org/) compatible [code viewer](code-viewer.md)

## Search
* [faceted search](faceted-search.md)
* [preconfigured indexes](index-configuration.md)
* Lucene string analyzer
  * configured for TEI
  * configuration and maintenance of charmaps and synonyms
  * Range Index

## TextGrid specific
* [TextGrid clients](tgclients.md)
  * pull data from the TextGrid Repository
  * publish data from the TextGridLab with SADE-Publisher
  * validation against RelaxNG schema on publish
  * store data in the TextGrid Repository
* usage of [IIIF image server from TextGrid (IIIF Proxy)](images.md)
* completely compatible with [TextGrid Metadata Scheme](https://wiki.de.dariah.eu/display/TextGrid/Metadata)

## development
* create [call graphs](callgraphs.md) of modules
* prepared for optional prerendering and caching of transformations
* [fork/download SADE projects](forking.md)

## Third party addons
* [SemToNotes](https://hkikoeln.github.io/SemToNotes/)
* [TEI stylesheets (XSLT)](http://www.tei-c.org/Tools/Stylesheets/)
* Markdown parser
