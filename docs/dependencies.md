# Dependencies

SADE uses some third party software, especially for its frontend design.
This encompasses:

* [TEI Stylesheets](http://www.tei-c.org/Tools/Stylesheets/)
* [Bootstrap](http://www.getbootstrap.com)
* [JQuery](https://jquery.com/)
* [highlight.js](https://highlightjs.org/)
* ... and several minor JavaScript files


__Attention:__

Said software is updated unfrequently.
While we make sure that the generic SADE works with updates, we do not guarantee that it is backwarts compatible.
In case you have forked SADE some time ago and want to update your projects, please keep in mind that this might lead to compatibility problems.

Please consult the [SADE assets](https://gitlab.gwdg.de/SADE/assets) the find out which version of third party software is used respectively.
