# Multilingual

This module enables multilingual content. By default, German is set as the default/main language and English as an alternative.

The module works with the GET parameter "lang" which is automatically attached to internal links. This module is compatible with the module "Confluence Wiki". To display language-specific content in Confluence, please use Confluence's own macros (e.g. German, English, ...).

## Configuration
Open `config.xml` to change configuration.
There, check out the module `multilanguage`.
The parameter `lang.enabled` must be set to 'true' to enable this module.
If enabled, the parameter `lang.default` must contain a default language (or the corresponding language abbreviation).
An unlimited number of additional languages can be added to `lang.alt`; These languages must be separated by a semicolon.
The following example configures the languages German (default), English and French (alternatives):

```xml
<module key="multilanguage">
  <param key="lang.enabled">true</param>
  <param key="lang.default">de</param>
  <param key="lang.alt" description="a semicolon seperated list of alternative languages">en;fr</param>
</module>
```
If you want to use the built-in HTML Language Switcher, you have to configure language names for each language code:

Open `lang.xml` and go to key="Language". The default configuration looks like this:

```xml
<word key="Language">
    <lang key="de">Deutsch</lang>
    <lang key="en">English</lang>
    <lang key="fr">Français</lang>
</word>
```

## Usage
If you want to use the built-in navigation bar, you have to open `navigation.xml` and configure one label for each language.

The following example applies to German as the default language and English as the alternative language:

```xml
<submenu label="Projektbeschreibung" label-en="project description">
```

To display individual words, such as headings, in different languages use `div` or `span` elements, set
the parameter `data-template="lang:translate"` and choose a new `data-template-content` parameter. Example:

```html
<span data-template="lang:translate" data-template-content="Search"/>
```

This `data-template-content` parameter must be unique and added to file `lang.xml`.
The following example shows the translations for the word "Search" in German, English and French:

```xml
<word key="Search">
  <lang key="de">Suche</lang>
  <lang key="en">Search</lang>
  <lang key="fr">Recherche</lang>
</word>
```
