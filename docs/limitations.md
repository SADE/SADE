# Limitations

## Documentation structure and URL rewriting
A flat hierarchy must be applied to the  [`/docs`](./) directory and so there are no
directories allowed within. The reason for this is that we want to be able to view the
documentation within the git repo\`s web interface and on the SADE website.
Therefore we use a set of redirections and forwards within the `controller.xql`.

The documentation is available via direct URL like `/docs/about.md`. Additionally
the markdown files are parsed via `content.html?id=/docs/about.md`, but images
(or other binary data) is available only when using the direct URL mentioned at
first.
