# SADE
SADE, an acronym for «Scalable Architecture for Digital Editions», is a software
application for the XML database [eXist-db](https://exist-db.org). It includes many [components](features.md) which are particularly suitable for developing and displaying a digital edition. It focuses on the presentation of
XML documents that follow the guidelines of the Text Encoding Initiative ([TEI](http://www.tei-c.org)).


## TextGrid flavored SADE
This version is prepared to present data transferred via the [TextGrid](https://textgrid.de/)
Laboratory. You can use the [SADE Publish Tool Plug-In](https://wiki.de.dariah.eu/display/TextGrid/Publish+Tool+SADE) in the Lab for an easy data ingest. However, SADE can be run without using TextGrid at all.


## Features
SADE currently offers several features that help you set up a digital edition, e.g.:

* a [faceted search](faceted-search.md)
* a tool for display [multiple languages](multilingual.md) on the website
* a [highlight.js](https://highlightjs.org/) compatible [code viewer](code-viewer.md)

and many more.

For a full overview see [SADE's feature list](features.md).


## Development

Get an overview of the complete architecture and an entry point to start
developing right now at the [Developer's Guide](develop.md).

You can find our git repositories at
[GWDG's Gitlab](https://gitlab.gwdg.de/SADE/).

We are happy to get your feature requests and bug reports!


## History
Since 2012 a larger developer community has constantly been working on this
software, which has originally been developed at the [Berlin-Brandenburg Academy of
Sciences and Humanities (BBAW)](http://www.bbaw.de/) as part of the digitization initiative «The
Electronic Life Of The Academy» ([TELOTA](http://www.bbaw.de/en/telota)).
Contributors are among others the [BBAW](http://www.bbaw.de/), the [Austrian Academy of Sciences](https://www.oeaw.ac.at) (OeAW; as part of the project CLARIN: Common Language Resources and Technology Infrastructure), the [Max Planck Institute for the History of Science ](https://www.mpiwg-berlin.mpg.de) (MPI-WG), the [Cologne Center for
eHumanities](http://cceh.uni-koeln.de) (CCEH) and the [Göttingen State and University Library](https://www.sub.uni-goettingen.de) (SUB; as part of the [TextGrid](http://www.textgrid.de) project).

This TextGrid flavored SADE has been largely refactored, updated and further developed since 2015 by Göttingen State and University Library.
