# Publish via the TextGridLab

Within the TextGridLab the SADE Publish-Plugin is needed for tranferring data to
SADE easly. It is available via the [Marketplace](https://wiki.de.dariah.eu/display/TextGrid/Marketplace).

## Installation
![Marketplace Screenshot](~assets/docs/marketplace.png)

## Configuration
After the installation process is done, the plugin needs a pointer to a SADE
server. Add the configuration via menu "Window" -> "Preferences"

![Preferences Screenshot](~assets/docs/preferences.png)

* __SADE Url:__ http://localhost:8080/exist/apps/sade/publish.html
* __Authorized User:__ admin
* __Password:__

(Leave the password empty for now, but keep in mind that any software not
  secured with a proper password is a risk for the internet at large.)

## Usage
To publish data from the Lab to a SADE instance the SADE server must be up and
running. If this is not the case so far, please start the server with `start.jar`
(Windows) or `bash bin/startup.sh`.

### Start the Publish tool
To start the SADE publisher use on of the following options:

* click the icon ![icon](~assets/docs/publisher-icon.png),
* via menu `Tools` -> `SADE Publish` or
* hit `[CTRL] + 3` and type `SADE` and select `SADE Perspective` from the list.

The view should change to something like this:
![SADE-Publish Perspective Screenshot](~assets/docs/sade-publish2.png)

When using the [Navigator's](https://wiki.de.dariah.eu/display/TextGrid/Navigator)
context menu entry `Publish to SADE` when the tool is not active, the item to be
published will not be recognized by the Publisher. You have to repeat the selection.

### Add items to publish
A context menu entry is added to the [Navigator](https://wiki.de.dariah.eu/display/TextGrid/Navigator). So you
can right-click on the item you want to publish and select `Publish to SADE`.
By holding `[CTRL]` or `[Shift]` you can select multiple items.

Linux and Mac users can also take advantage of a drag'n'drop feature. Simply drop
the items you want to publish at the central space within the publishers view.

When an aggregation is added to the Publisher, its content (and all of its content)
will be sent to SADE.

### Starting the process
Click on the "Publish" button. Successfully transferred items will be highlighted
green. The "Log" contains all information to the process.

### Where to find your data
All published items can be found in the database at `/db/apps/sade/textgrid/`,
depending on which kind of object you have published. Published XML files are
available at `http://your-server/exist/apps/sade/textgrid:$URI`.
