# Customization

SADE has been built to fit the needs of many projects creating digital editions. A few
things can be set up by any user who is familiar with the basics of XML but for a deeper
customization, an adaption of some XQuery modules may become necessary. If you want to
start preparing your own instance, look at the `config.xml` file in the root collection
of the application to set up the basics.

## Top Menu / Navigation

### General

SADE provides an out-of-the-box mechanism to customize the website's top navigation. It
is possible to add simple **buttons** or **dropdown menus**. All necessary adjustments
can be made in the `/db/apps/sade/nagivation.xml` file.

The configuration for the navigation shipped with SADE is this:

```xml
<navigation>
    <item label="Über SADE" label-en="About SADE" link="docs/about.md"/>
    <submenu label="Dokumentation" label-en="Documentation">
        <item label="Installation" label-en="Install" link="docs/install.md"/>
        <item label="Features" label-en="Features" link="docs/features.md"/>
        ...
    </submenu>
    <submenu label="Weiterführende Links" label-en="Further Links">
        <item label="TextGrid Benutzerhandbuch" link="https://dev2.dariah.eu/wiki/display/TextGrid/User+Manual+2.0"/>
        <item label="TextGrid Homepage" link="http://textgrid.de"/>
    </submenu>
</navigation>
```

In case you want to add a new top level link to the navigation, simply add a new `item`
node as a child node of `navigation`, e.g.:

```xml
<item label="Metadata" link="view.html?id=metadata.md"/>
```

In the `label` attribute you can provide the link's text whereas the `link` attribute
holds the path to the target resource.

To create a label for a dropdown selection of links, add a `submenu` node as a child
node of `navigation` with as many `item`s as you wish. Each `item` will be rendered as
a link in the dropdown menu.

```xml
<submenu label="Features">
    <item label="Feature 1" link="some/path/feature1.md"/>
    <item label="Feature 2" link="some/path/feature2.md"/>
    <item label="Feature 3" link="some/path/feature3.md"/>
</submenu>
```

### Navigation labels and multilingual support

Both the `item` and `subitem` nodes have a `label` attribute from which the text to be
displayed is taken by default. In case you have [**multilanguage**](multilingual.md)
support enabled, you can add more labels for other languages, e.g. `label-en` or
`label-es`. Please note that these have to be listed in the multilanguage module
configuration at `config.xml` as well; otherwise switiching languages might not work.
