# Code-Viewer
This app serves to display XML or (X)HTML code with syntax highlighting on a website.
It is fully compatible with any CSS themes provided by [highlight.js](https://highlightjs.org/) but without the client-sided JavaScript rendering that leads to performance losses while displaying large files.

Its main purposes are to

1. remove the dependency on JavaScript and by that providing more security
2. take load away from the client and let the server do all the work

This module has originally been developed for the DFG-funded project [Bibliothek der Neologie](https://www.bdn-edition.de) and spun off as a generic app for [SADE](https://gitlab.gwdg.de/SADE). It is **not** installed by default but can simply be added during the installation process of SADE.

For detailed information on how to install the app see [its README on GitLab](https://gitlab.gwdg.de/SADE/code-viewer/blob/master/README.md).


## Sample output

To see the Code Viewer in action we encourage you to visit the code view of [Theodor Fontane's notebook C7](https://fontane-nb.dariah.eu/xml.html?id=/xml/data/16b00.xml) which uses the [Visual Studio style](https://github.com/highlightjs/highlight.js/blob/master/src/styles/vs.css) of [highlight.js](https://highlightjs.org/).
