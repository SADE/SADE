# Prerendering
When the amount of data increases there will be a trade-off between boosting
the transformation with more efficient queries together with a corresponding
index configuration and the option to disable on-the-fly processing by caching
transformation results. It will speed up the page loading time.

The preprocessed items can be added to the database and may be available via REST
in addition to the original dataset.

When dealing with objects transferred from the TextGrid Repository (e.g. by the
SADE Publisher) there is exactly one place to insert a switch to the preferred
routines: within the `/modules/textgrid/connect.xqm` you can add instructions
based on the MIME-type of the source starting at [line 118](https://gitlab.gwdg.de/SADE/SADE/blob/ee5f711339a71b3ab500d1bb8cea6ab76a93be48/modules/textgrid/connect.xqm#L118)
 and with a placeholder at [line 135](https://gitlab.gwdg.de/SADE/SADE/blob/ee5f711339a71b3ab500d1bb8cea6ab76a93be48/modules/textgrid/connect.xqm#L135).

Prerendering is disabled by default, as it is hard to guess what transformation
should be done and where to store its output.
