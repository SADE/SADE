# Development

## Architecture
![architecture](~assets/diagram.svg)
This image includes draw.io data.

## Packages

SADE consists of three EXPath packages to run within the eXist-db
environment:

* SADE.xar - the main application ([git](https://gitlab.gwdg.de/SADE/SADE) | [built](https://ci.de.dariah.eu/exist-repo/packages.html?package-id=SADE))
* assets.xar - all static resources ([git](https://gitlab.gwdg.de/SADE/assets) | [built](https://ci.de.dariah.eu/exist-repo/packages.html?package-id=sade_assets))
* search engine - to configure the Lucene index ([git](https://gitlab.gwdg.de/fontane-notizbuecher/fontane-lucene-exist-module) | [built](https://ci.de.dariah.eu/exist-repo/public/fontane-lucene-exist-module-2.0.2.xar))

## Dependencies and Requirements
Within eXist-db the following packages are required:

* Markdown Parser
* Function Documentation
* Shared Resources

Further dependencies may be defined by [eXist-db](http://exist-db.org/exist/apps/doc/quickstart.xml).
Mainly this is Java 8 and sufficient amount of memory 2GiB should be available
for exclusive usage (`Xmx`).

## Build
There are `ant` tasks available in a separate [git repo](https://gitlab.gwdg.de/SADE/build). This build script generates a complete package containing the eXist database and all required components. Configure this build via the `generic.build.properties` file.

Every package can be built separately by calling `ant` in the root directory. The
result will be a `build\*.xar` file.

To simplify development you can get a quick set up of the current SADE application on your file system by invoking `ant test`, which creates a runnable database at `test/eXist{$version}/bin/startup.sh`.

## Physical Structure
The file structure is pretty usual for EXPath application within eXist-db.
To read more on this, the chapter about the Model View Controller in the eXist-db
book is highly recommended.

All basic files are stored in the app root directory, especially the main configuration
and the controller.xql (the main router in the MVC model), index configuration
and package descriptions.

```
   root
     |-- docs
     |-- modules
     |-- templates
     |-- textgrid
```

## Request Processing by Example
All requests for HTML pages are handled by the `controller.xql`. Please see the
documentation of eXist-db on [MVC and Pipelines](http://exist-db.org/exist/apps/doc/urlrewrite.xml#D3.29).

## Configuration
All required and additional parameters are set within `config.xml`. Parameters
are parsed by the config module (`config.xqm`). The most demanded function is
`config:get($key)` and `config:get($key, $module)`. They will return any data
stored in the requested part in `config.xml`.
