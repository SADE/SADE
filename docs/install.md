# Install SADE

## Debian / Ubuntu

SADE is provided as Debian package for Debian based Linux distributions like Ubuntu.

### Installation from command line

Latest snapshot development packages can be downloaded from
[https://ci.de.dariah.eu/packages/pool/snapshots/s/sade/](https://ci.de.dariah.eu/packages/pool/snapshots/s/sade/)

After downloading the latest Debian package, install it from command line

```sh
sudo dpkg -i sade_*.deb
```

while selecting the appropriate version and date of the package. After the package's
installation, eXist-db fires up and is available at [http://localhost:8080/](http://localhost:8080/).

### Installation with Debian's Software Center

Another option to install SADE is using Debian's Software Center. First, download a SADE
Debian package as described above and double click on it from your desktop environments
file manager. The Software Center should launch and guide you through the installation.

As soon as the installation is complete, eXist-db takes a moment to fire up and is
then available at [http://localhost:8080/](http://localhost:8080/).

## All Operating Systems

Make sure you have [Java](https://www.java.com/de/download/) installed at least in version
1.8.

Download SADE as a zip file:

* [latest release](https://gitlab.gwdg.de/SADE/build/-/jobs/artifacts/master/download?job=build-master) - recommended
* [lastest unstable and untested build](https://gitlab.gwdg.de/SADE/build/-/jobs/artifacts/develop/download?job=build-develop)

Unzip the `artifacts.zip` file and change to the `build/sade/bin` directory.
Start the service with `./startup.sh` (Linux/Mac) or `startup.bat` (Windows).
If you are experiencing problems starting SADE this way you may also try to change to the
`build/sade` directory and enter `java -jar start.jar jetty` from the command line.

Finally, eXist-db's front and back end are available at [http://localhost:8080/](http://localhost:8080/)
and the local SADE is visible at
[http://localhost:8080/exist/apps/sade/index.html](http://localhost:8080/exist/apps/sade/index.html).

## How to log in

SADE is initialized with a default superuser `admin` with an empty password.

__Caution:__ Provide the superuser `admin` with a password, if you plan to deploy your
SADE instance to a server.
