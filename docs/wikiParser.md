# WikiParser
This module loads, parses and caches documents from wiki systems. It gets the
data via export function (Dokuwiki) or REST interfaces (Confluence), transforms
the somewhat weird formats to plain HTML and stores these resources in `docs`.

To setup one or both modules, a specified configuration is needed. Continue reading at the
specific documentation:

* [Confluence Wiki](confluenceParser.md)
* [DokuWiki](dokuwikiParser.md)
