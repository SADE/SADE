# Confluence wiki parser

This module is able to to parse **public** wiki pages of a Confluence wiki and displays them as HTML pages.
It is fully compatible with the [multilanguage module](multilingual.md).

## Configuration

The parser can be configured at `config.xml` where the URL of the wiki's REST interface has to be stated:

```xml
<param key="confluence.url">https://wiki.de.dariah.eu/rest/api/content/</param>
```

## Usage

After configuring you can insert the HTML serialization of a wiki page by adding the following code on the desired HTML page:

```html
<div data-template="confluence:confluence" data-template-id="$(some-id)"/>
```

The variable `$(some-id)` equals to the pageID of the wiki page to be parsed.
An example page, `wiki.html`, is available by default in the `templates` directory of SADE.
You can either change this page's `data-template-id` or pass a parameter to it, e.g. `wiki.html?id=44769550`.

## ... and in combination with the multilanguage module

In order to work properly, please make sure you alreade have configured the multilanguage module as described in the [module's documentation](multilingual.md).
By this the desired languages as well as their respective abbreviations are stated in the `config.xml`.
Now add a mapping for each abbreviation to `<module key="wiki">` in `config.xml` to the Confluence specific macro name of the language.

By default the following mapping is configured:

```xml
<param key="confluence.lang" description="if the multilanguage plugin is enabled, used languages specified here">
  <lang code="de" name="german"/>
  <lang code="en" name="english"/>
  <lang code="fr" name="french"/>
</param>
```

## How it works

On each page view the module checks if there is a new version of the wiki page available.
If this is the case the page gets parsed and saved.
In case the multilanguage module is activated the parser saves the page for each language stated.
