# DokuWikiParser

> This module was developed in the project on Theodor Fontanes Notebooks.
To use it with the current version of SADE and any other instance of DokuWiki,
some adjustments to the code are needed. (Approx. 8h for coding, 4h for testing
and 4h for documentation.)
If you like to use it, please get in contact with us. We will help you.

To be able to maintain the pages containing content about project, documentation
and other stuff a comfortable editor and a system that stores the history of a
document is very helpful. In many cases projects use wikis for communication,
coordination and as a general knowledge base. By adding selected pages to a
white-list, the wiki may become the place for editing the textual content of
the SADE webpage.

DokuWiki is a common wiki software. To include a page from DokuWiki the following
parameters in `config.xml` need adjustment:

```xml
<!-- sample configuration for DokuWiki -->
<param key="dokuwiki.url">https://dokuwiki.example.com</param>
<param key="dokuwiki.space">fontane</param>
<param key="dokuwiki.whitelist">
    <item>entry-page</item>
    <item>about</item>
    <item>contact</item>
</param>
```

The DokuWikiParser was developed by the project on [Fontane's notebooks](http://fontane-nb.dariah.eu).
All the code is in `modules/wiki/wiki.xqm`.
