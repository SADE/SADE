# How to search

After entering a search term a list of matching documents is presented.

## Facets

On the left side a list of terms is presented which allow to further filter the
result set. A facet could be presented like:

        Place of Publication
          - Munich  (14)
          - Berlin (8)

This tells that 14 documents have been published in Munich and 8 have been
published in Berlin. Clicking on the label name, e.g. "Munich" will just show
you the 14 documents which have been published in Munich. You could now apply
further filters, like on the "Publication Year". The filter can be removed with
clicking the small minus sign "-" in front of the selected term. If you do not
click on the term "Munich", but on the minus sign "-", all documents **not**
published in Munich are shown. After applying the filter, a small plus sign "+"
appears in front of the filter. This allows you to undo this filter again.
