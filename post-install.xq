xquery version "3.1";

declare namespace repo="http://exist-db.org/xquery/repo";
declare namespace xconf="http://exist-db.org/collection-config/1.0";
(: The following external variables are set by the repo:deploy function :)

(: the target collection into which the app is deployed :)
declare variable $target external;

(:~
 : Helper function to get all function names declared in the application.
 : Uses externally declared variable $target.
 :
 : @author Mathias Göbel
 : @since 3.1.0
 :)
declare function local:get-function-names()
as xs:string* {
for $modulePath in collection($target)/base-uri()[ends-with(., ".xqm")]
let $functions := inspect:module-functions( xs:anyURI($modulePath) ) (: QNames :)
return
  for $function in $functions
  let $functionQName := function-name($function)
  return
      prefix-from-QName($functionQName) || ":" || local-name-from-QName($functionQName)
};

(:~
 : Formats the test coverage output - mainly used by GitLab test coverage parser.
 :
 : @author Mathias Göbel
 : @since 3.1.0
 : @see https://docs.gitlab.com/ee/user/project/pipelines/settings.html#test-coverage-parsing
 :   :)
declare function local:format-test-rate-output($value as xs:decimal)
as xs:string {
    "coverage: " || ($value * 100) => format-number("###.00") || "%"
};

(:~
 : Returns the relative amount of tests for all function in this package
 : @param $tests – a set of unit test results in the JUnit XML format
 :
 : @author Mathias Göbel
 : @since 3.1.0
 : @see https://www.ibm.com/support/knowledgecenter/en/SSQ2R2_14.1.0/com.ibm.rsar.analysis.codereview.cobol.doc/topics/cac_useresults_junit.html
 :  :)
declare function local:test-function-rate($tests as element(testsuites)*)
as xs:string {
    (: is function cardinality an issue? thats why disstinct-value is used :)
    let $functionNames := (local:get-function-names()) => distinct-values() => count()
    let $testedFunctionNames := ($tests//testcase/string(@class)) => distinct-values() => count()
    let $coverage :=
        if($testedFunctionNames = 0) then 0 else $testedFunctionNames div $functionNames
    return
        local:format-test-rate-output($coverage)
};

(:~
 : Returns the relative amount of successfull tests
 : @param $tests – a set of unit test results in the JUnit XML format
 :
 : @author Mathias Göbel
 : @since 3.1.0
 : @see https://www.ibm.com/support/knowledgecenter/en/SSQ2R2_14.1.0/com.ibm.rsar.analysis.codereview.cobol.doc/topics/cac_useresults_junit.html
 :  :)
declare function local:test-success-rate($tests as element(testsuites)*)
as xs:string {
  let $allFailures := $tests//testsuite/@failures => sum()
  let $failsTriggered := $tests//testcase[@name=("FAIL", "fail")] => count()
  let $failures := $allFailures - $failsTriggered
  let $errors := $tests//testsuite/@errors => sum()
  let $pending := $tests//testsuite/@pending => sum()

  let $testsDone := $tests//testsuite/@tests => sum()

  let $testCoverage := ($testsDone - sum( ($failures, $errors, $pending) )) div $testsDone
  return
    local:format-test-rate-output($coverage)
};

let $project-name := tokenize($target, "/")[last()]
let $log := util:log-system-out("installing " || $project-name)

let $fileSeparator := util:system-property("file.separator")
let $system-path := system:get-exist-home() || $fileSeparator
let $use-template :=
    if(doc-available($target || "/textgrid/data/collection-template.xconf"))
    then xmldb:rename($target || "/textgrid/data", "collection-template.xconf", "collection.xconf")
    else true()
let $data-xconf := $target || "/textgrid/data/collection.xconf"
let $store-xconf := xmldb:store("/db/system/config" || $target || "/textgrid/data", "collection.xconf", doc( $data-xconf ))
let $reindex := xmldb:reindex($target || "/textgrid/data")

(: we have to test for writable path :)
let $path := $system-path
let $user := util:system-property("user.name")
let $message1 := $path || " is not available. Create it and make sure "
                || $user || " can write there."
let $message2 := "Could not write to " || $path || "."
return (
if
    (file:is-directory($path))
then
    try { file:serialize-binary( xs:base64Binary(util:base64-encode("beacon")) , $path || "beacon.txt") }
    catch * { util:log-system-out( "&#27;[48;2;"|| "255;0;0" ||"m&#27;[38;2;0;0;0m " ||
    $message2 || " &#27;[0m" ) }
else
    util:log-system-out( "&#27;[48;2;"|| "255;0;0" ||"m&#27;[38;2;0;0;0m " ||
    $message1 || " &#27;[0m" )
,

let $parameters :=
    <output:serialization-parameters
        xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
        <output:method value="xml"/>
        <output:indent value="yes"/>
    </output:serialization-parameters>

let $tests := util:eval(xs:anyURI('test.xq'))
let $print := util:log-system-out(serialize($tests, $parameters) )
let $print := util:log-system-out( local:test-function-rate($tests) )
let $file-name := $system-path || ".."|| $fileSeparator || "tests-"
    || $project-name || "_job-" || environment-variable("CI_JOB_ID") || ".log.xml"

let $file := file:serialize(<tests time="{current-dateTime()}">{ $tests }</tests>, $file-name, ())
  return
    util:log-system-out($file-name || " saved.")
)
