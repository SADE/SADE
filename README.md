# SADE – Scalable Architecture for Digital Editions

SADE ( Scalable Architecture for Digital Editions) it an application for
[eXist-db](https://exist-db.org) that offers a set of useful features to quickly build a website for digital editions basing on [TEI](http://www.tei-c.org)-encoded texts.

This is the main application for SADE. It serves as XAR package according to the
[EXPath Packaging System](http://expath.org/spec/pkg) and is created as an application for
[eXist-db](https://exist-db.org).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* ant >= 1.10.0

### Local installation

To get a version of SADE running locally on your computer, clone this repository in your preferred git directory:

```sh
mkdir SADE
cd SADE
git clone git@gitlab.gwdg.de:SADE/SADE.git
```

Once the repository is cloned, run

```sh
./build_test
```

which installs all necessary dependencies and creates a local version of SADE in `/test`.
To start the database, run

```sh
bash test/exist-distribution-*/bin/startup.sh
```

SADE's backend is now available at `localhost:8080`. To have a look at the website, view `http://localhost:8080/exist/apps/sade/index.html` in your preferred browser.

## The Git repo

This repo contains the sources of the application which contain a brief
documentation as long as you are not dealing with a fork. Please find the
documentation in the folder `docs` where they are stored in Markdown. Any
default installation of this package serves the documentation as website
integrated in your template and in your instance.

### Git hooks

The folder `.hooks` contains git hook scripts to be executed on certain git
events. The usage of these scripts is recommended, as they ensure a smooth
workflow.

#### pre-commit

We test for `DONOTCOMMIT` comments in the code. As long as a comment like this
is in files, no commit is possible.

#### post-commit

On any commit a new artifact is being created, so you can proceed and test it
immediately.

## Build

```sh
./build_xar
```

Artifacts will be stored in `.build/`.

The default artifact is the xar package.

## Test

`./build_test` will download the specified version of eXist and all dependencies to
a ready-to-go instance in the `test/` directory. at the first start all tests
will be executed by the `post-install.xq` script. To start the database use

```sh
test/exist-distribution-*/bin/startup.sh
```

Guide your favorite browser to the [dashboard](http://localhost:8080/exist/).

### Port settings

When the port 8080 is blocked by a different service, simply set different ones
with the following command and restart the database.

```sh
sed -i 's/"8080"/"8090"/g; s/"8443"/"8444"/g' \
  test/eXist-db-4.5.0/tools/jetty/etc/jetty-http.xml \
  test/eXist-db-4.5.0/tools/jetty/etc/jetty.xml \
  test/eXist-db-4.5.0/tools/jetty/etc/jetty-ssl.xml
```

## Known Issues

In the unlikely event of cycling to the next digit in TextGrid URIs you may
encounter issues when you going to use an older URI together with a newer that
starts with the same characters. Example: One will publish the text
`textgrid:foo.0` and over time the URI number increases that much, that
another object that should be stored in the database has the URI
`textgrid:foobar.0`, some processes that tests against using `starts-with()`
will fail or may respond with the wrong object. At the end of 2017 we are
dealing with 5 digits of Latin letters and Arabic numbers (a total of 36
different symbols) starting with "3". So it will take another approx. ten
million items to reach the next digit. So we will talk about this later.
