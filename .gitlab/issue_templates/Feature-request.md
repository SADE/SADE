# Description

Just leave a short summary what the feature is about.

# Use Cases

If you can, please provide use cases for this feature.

# Classification

Is this feature an enhancement of existing code or a completely new feature?

* [ ] enhancement
* [ ] new feature

# Related Tickets

Add all related issues if applicable.

/cc [Mathias Göbel](https://gitlab.gwdg.de/mgoebel), [Stefan Hynek](https://gitlab.gwdg.de/hynek), [Michelle Weidling](https://gitlab.gwdg.de/mrodzis)
