## [4.1.1](https://gitlab.gwdg.de/SADE/SADE/compare/v4.1.0...v4.1.1) (2021-05-10)


### Bug Fixes

* make TILE publishable ([bbe883b](https://gitlab.gwdg.de/SADE/SADE/commit/bbe883be560fa406870d26910427bcb15b17d906))

# Changelog

This project's changelog is maintained in the database's [`repo.xml`](https://gitlab.gwdg.de/SADE/SADE/blob/develop/repo.xml).
