xquery version "3.1";
(:~
 : Script providing access to the test functions (XQSuite). Called
 : at post-install stage, evaluated at CI.
 :)

import module namespace test="http://exist-db.org/xquery/xqsuite" at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";
import module namespace tests="https://sade.textgrid.de/ns/tests" at "test.xqm";

import module namespace callgraph-test = "https://sade.textgrid.de/ns/callgraph-test" at "modules/callgraph/tests/callgraph-test.xq";
import module namespace inspect2graphml-test = "https://sade.textgrid.de/ns/inspect2graphml-test" at "modules/callgraph/tests/inspect2graphml-test.xq";
import module namespace tgclient="https://sade.textgrid.de/ns/tgclient" at "/db/apps/sade/modules/textgrid/client.xqm";


test:suite(util:list-functions("https://sade.textgrid.de/ns/tests")),

(: * CALLGRAPH * :)
test:suite(util:list-functions("https://sade.textgrid.de/ns/callgraph-test")),
test:suite(util:list-functions("https://sade.textgrid.de/ns/inspect2graphml-test")),

(: * TGCLIENT * :)
test:suite(util:list-functions("https://sade.textgrid.de/ns/tgclient"))
